({
    requiredFieldValidation : function(component,event) {
        var allRecords = component.get("v.AccountList");
        var isValid = true;
        for(var i = 0; i < allRecords.length;i++){
             //alert(allRecords[i].backend_due_date__c);
            if(allRecords[i].Subject == null || allRecords[i].Subject == undefined){
                if(allRecords[i].Subject.trim() == ''){
                    alert('fill this field : Row No ' + (i+1) + ' Account Name is null' );
                    isValid = false;
                   
                } 
            }
        }
        return isValid;
    },
})