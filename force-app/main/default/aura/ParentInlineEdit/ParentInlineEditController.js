({
    getAccountsJS : function(component, event, helper) {
        var action = component.get("c.accountsToDisplay");
        action.setParams({'recordId': component.get("v.recordId")})
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS" && component.isValid()) {
                component.set("v.AccountList", response.getReturnValue());
            }
            //alert(response.getReturnValue().length);
        });
        
        var action1 = component.get("c.accountsToDisplay");
        action1.setParams({'recordId': component.get("v.recordId")})
        action1.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS" && component.isValid()) {
                if(response.getReturnValue() != null){
                    component.set("v.oppId", response.getReturnValue());
                }
            }
        });
        
        $A.enqueueAction(action);
        // $A.enqueueAction(action1);
    },
    SaveAccount : function(component, event, helper) {
        if (helper.requiredFieldValidation(component, event)){
            var action = component.get("c.saveAccount");
            action.setParams({
                'listOfAccount': component.get("v.AccountList")
            });
            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS" && component.isValid()) {
                    component.set("v.AccountList", response.getReturnValue());
                    console.log('The val is:'+JSON.stringify(response.getReturnValue()));
                    component.set("v.showSaveCancelBtn",false);
                    alert('Task Updated...');
                    //$A.get('e.force:refreshView').fire();
                }
            });
            $A.enqueueAction(action);
           
        }
    },
})