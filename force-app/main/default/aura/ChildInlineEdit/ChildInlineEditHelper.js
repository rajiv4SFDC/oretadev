({
   // fetch picklist values dynamic from apex controller
    fetchPickListVal: function(component,event,helper, fieldName, picklistOptsAttributeName) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.objInfoForPicklistValues"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
           /****
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }  ****/
                opts.push({ label: "All", value: true });
                var tempSelcted = component.get("v.singleAccount.Status");
                console.log('The tempSelcted  is :'+tempSelcted);
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.set("v." + picklistOptsAttributeName, opts);
                
            }
        });
        $A.enqueueAction(action);
    },
    assignColorCodes : function(component,event,helper){
        var statusValue = component.get('v.singleAccount').Status;
        
        //alert('From helper : '+component.get('v.singleAccount').OwnerId+'  :  ' +component.get('v.singleAccount').Owner.Name +'\nDueDate : '+component.get('v.singleAccount').backend_due_date__c);
        if(component.get('v.singleAccount').OwnerId != null || component.get('v.singleAccount').OwnerId != undefined || component.get('v.singleAccount').OwnerId !=''){
            component.set('v.userName',component.get('v.singleAccount').Owner.Name);
        }
        
        if(statusValue == 'Completed'){
            component.set('v.colorCode',"background-color:#32cd32; color:black");
        }
        else if(statusValue == 'Planned'){
            component.set('v.colorCode',"background-color:#cd5c5c; color:Black;");
        }
        else if(statusValue == 'In Progress'){
            component.set('v.colorCode',"background-color:pink; color:black");
        }
        else if(statusValue == 'On Hold'){
            component.set('v.colorCode',"background-color:#ffff00; color:#000000");
        }
        else if(statusValue == 'Deferred'){
            component.set('v.colorCode',"background-color:#ffff00; color:#000000");
        }
        //alert(component.get('v.colorCode'));
    }
})