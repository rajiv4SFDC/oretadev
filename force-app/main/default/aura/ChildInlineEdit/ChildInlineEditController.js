({
    doInit: function(component, event, helper) {
        //alert('Id : '+component.get('v.singleAccount').Id);
        var ids = component.get('v.singleAccount').Id;
        var action = component.get('c.timeLineValues');
        action.setCallback(this,function(response){
            if(response.getState() === 'SUCCESS'){
                var result = response.getReturnValue();
                component.set('v.timeLineMap',result);
                //alert('from controller '+response.getReturnValue()[component.get('v.singleAccount').Id]);
                for(var x in result){
                    //alert('from controller '+result[x]);
                }
               //alert('alert from controller : '+component.get('v.timeLineMap')[ids]);
                component.set('v.timeLineDays',component.get('v.timeLineMap')[ids]);
            }
        });
        $A.enqueueAction(action);
        
        helper.assignColorCodes(component,event,helper);
        helper.fetchPickListVal(component,event,helper, 'Status', 'ratingPicklistOpts');
    },
    EditName : function(component,event,helper){  
        component.set("v.nameEditMode", true);
        setTimeout(function(){
            component.find("inputId").focus();
        }, 100);
    },
    EditComments : function(component,event,helper){  
        component.set("v.nameEditMode", true);
        setTimeout(function(){
            component.find("inputComments").focus();
        }, 100);
    },
    
    
    inlineEditRating : function(component,event,helper){  
        component.set("v.ratingEditMode", true);
        component.find("accRating").set("v.options" , component.get("v.ratingPicklistOpts"));
        setTimeout(function(){
            component.find("accRating").focus();
        }, 100);
    },
    inlineEditAssignedTo : function(component,event,helper){  
        component.set("v.assignedTo", true);
        //component.find("accOwnerId").set("v.options" , component.get("v.ratingPicklistOpts"));
        setTimeout(function(){
            component.find("accOwnerId").focus();
        }, 100);
    },
    
    onNameChange : function(component,event,helper){
        if(event.getSource().get("v.value").trim() != ''){
            component.set("v.showSaveCancelBtn",true);
        }
    },
    onRatingChange : function(component,event,helper){
        component.set("v.showSaveCancelBtn",true);
        component.set("v.ratingEditMode", false);
    },    
    closeNameBox : function (component, event, helper) {
        component.set("v.nameEditMode", false);
        if(event.getSource().get('v.value') != undefined){
            if(event.getSource().get("v.value").trim() == ''){
                component.set("v.showErrorClass",true);
            }else{
                component.set("v.showErrorClass",false);
            }    
        }
        
    },
    closeRatingBox : function (component, event, helper) {
        component.set("v.ratingEditMode", false);
    },
    closeAssignedToBox : function (component, event, helper) {
        component.set("v.assignedTo", false);
    },
})