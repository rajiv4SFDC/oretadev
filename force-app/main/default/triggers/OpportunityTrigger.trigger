trigger OpportunityTrigger on Opportunity (after update, before update) {

     if(Trigger.isUpdate && Trigger.isAfter)
     opportunityTriggerHandler.afterUpdateHandler(Trigger.New,Trigger.OldMap);
 
     if(trigger.isUpdate && trigger.isBefore)
     opportunityTriggerHandler.beforeUpdateHandler(Trigger.New,Trigger.OldMap);
}