trigger ChecklistTrigger on Opportunity_Product__c (after insert) {


  // Written code at mid night.  Bulkify and beautify and create helper class later. 
  
  if(Trigger.Isinsert && Trigger.isAfter)
  {
     // Create Checklist task record for basing on the recordType Name. 
      for(Opportunity_Product__c op1:trigger.New)
      {  
        system.debug('The op1.RecordTypeId is :'+op1.RecordTypeId);
         if(op1.RecordTypeId == Label.Mobile_PPN_TNB_NEW)
          {
          Checklist_task__c ct = new Checklist_task__c();
  ct.Product__c = op1.Product__c;
  ct.Opportunity__c = op1.Opportunity__c;
  if(op1.Opportunity__c != NULL)
  ct.Account__c = [select id,accountid from opportunity where id=:op1.Opportunity__c].Accountid;
  
ct.Product_Workflow_Checklist__c = op1.Id;
ct.RecordTypeId =  Schema.SObjectType.Checklist_task__c.getRecordTypeInfosByName().get('Mobile PPN and TMB NEW').getRecordTypeId();
ct.Assigned_To_1__c = '0050I000008ymEk';
ct.Assigned_To_2__c = '0050I000008ymEk';
ct.Assigned_To_3__c = '0050I000008ymEk';
ct.Assigned_To_4__c = '0050I000008ymEk';
ct.Assigned_To_5__c = '0050I000008ymEk';
ct.Assigned_To_6__c = '0050I000008ymEk';
ct.Assigned_To_7__c = '0050I000008ymEk';
ct.Assigned_To_8__c = '0050I000008ymEk';
ct.Assigned_To_9__c = '0050I000008ymEk';
ct.Assigned_To_10__c = '0050I000008ymEk';
ct.Assigned_To_11__c = '0050I000008ymEk';
ct.Due_Date_1__c = System.Now().addHours(2);
ct.Due_Date_2__c = System.Now().addHours(4);
ct.Due_Date_3__c = System.Now().addHours(6);
ct.Due_Date_4__c = System.Now().addHours(8);
ct.Due_Date_5__c = System.Now().addHours(10);
ct.Due_Date_6__c = System.Now().addHours(12);
ct.Due_Date_7__c = System.Now().addHours(14);
ct.Due_Date_8__c = System.Now().addHours(16);
ct.Due_Date_9__c = System.Now().addHours(18);
ct.Due_Date_10__c = System.Now().addHours(20);
ct.Due_Date_11__c = System.Now().addHours(22);
ct.Priority_1__c = 'High';
ct.Priority_2__c = 'High'; 
ct.Priority_3__c =  'High';
ct.Priority_4__c =  'High';
ct.Priority_5__c =  'High';
ct.Priority_6__c =  'High';
ct.Priority_7__c =  'High';
ct.Priority_8__c =  'High';
ct.Priority_9__c =  'High';
ct.Priority_10__c =  'High';
ct.Priority_11__c =  'High';
ct.Status_1__c = 'Planned';
ct.Status_2__c = 'Planned';
ct.Status_3__c = 'Planned';
ct.Status_4__c = 'Planned';
ct.Status_5__c = 'Auto Update';
ct.Status_6__c = 'Planned';
ct.Status_7__c = 'Planned';
ct.Status_8__c = 'Planned';
ct.Status_9__c = 'Planned';
ct.Status_10__c = 'Planned';
ct.Status_11__c = 'Planned';
ct.Task_Subject_1__c = 'Confirm that there are 100 points of ID uploaded into the vault';
ct.Task_Subject_2__c = 'Confirm the customer is an authorised representative';
ct.Task_Subject_3__c = 'Confirm that the signed paperwork/voice recording is on file';
ct.Task_Subject_4__c = 'Confirm that the paperwork/voice recording matches the details in the data sheet';
ct.Task_Subject_5__c = 'Copy and paste details from data sheet, attach the critical information summary and send the email to the customera';
ct.Task_Subject_6__c = 'Order stock (if required) and send email with estimated stock arrival time';
ct.Task_Subject_7__c = 'Process through MNC';
ct.Task_Subject_8__c = 'Upload generated contract';
ct.Task_Subject_9__c = 'Book the courier, add job number and send customer tracking number';
ct.Task_Subject_10__c = 'ClickPOS sale';
ct.Task_Subject_11__c = 'Set time for NPS email to be sent';
insert ct;   
          }
          
     else if(op1.RecordTypeId == Label.DOT_NBN_CheckListTask)
       {
       system.debug('going inside here:');
        Checklist_task__c ct = new Checklist_task__c();
       
  ct.Product__c = op1.Product__c;
  ct.Opportunity__c = op1.Opportunity__c;
  if(op1.Opportunity__c != NULL)
  ct.Account__c = [select id,accountid from opportunity where id=:op1.Opportunity__c].Accountid;
  
  ct.Product_Workflow_Checklist__c = op1.Id;
ct.RecordTypeId =  Schema.SObjectType.Checklist_task__c.getRecordTypeInfosByName().get('DOT NBN').getRecordTypeId();


  ct.Assigned_To_1__c = '0059D000000Z2K1';
ct.Assigned_To_10__c =  '0059D000000Z2K1';
ct.Assigned_To_11__c =  '0059D000000Z2K1';
ct.Assigned_To_12__c =  '0059D000000Z2K1';
ct.Assigned_To_13__c =  '0059D000000Z2K1';
ct.Assigned_To_14__c =  '0059D000000Z2K1';
ct.Assigned_To_15__c =  '0059D000000Z2K1';
ct.Assigned_To_16__c =  '0059D000000Z2K1';
ct.Assigned_To_17__c =  '0059D000000Z2K1';
ct.Assigned_To_2__c =  '0059D000000Z2K1';
ct.Assigned_To_3__c =  '0059D000000Z2K1';
ct.Assigned_To_4__c =  '0059D000000Z2K1';
ct.Assigned_To_5__c =  '0059D000000Z2K1';
ct.Assigned_To_6__c =  '0059D000000Z2K1';
ct.Assigned_To_7__c =  '0059D000000Z2K1';
ct.Assigned_To_8__c =  '0059D000000Z2K1';
ct.Assigned_To_9__c =  '0059D000000Z2K1';
ct.Due_Date_1__c =  System.Now().addHours(2);
ct.Due_Date_10__c = System.Now().addHours(4);
ct.Due_Date_11__c = System.Now().addHours(6);
ct.Due_Date_12__c = System.Now().addHours(8);
ct.Due_Date_13__c = System.Now().addHours(10);
ct.Due_Date_14__c = System.Now().addHours(12);
ct.Due_Date_15__c = System.Now().addHours(14);
ct.Due_Date_16__c = System.Now().addHours(16);
ct.Due_Date_17__c = System.Now().addHours(18);
ct.Due_Date_2__c = System.Now().addHours(20);
ct.Due_Date_3__c = System.Now().addHours(22);
ct.Due_Date_4__c = System.Now().addHours(24);
ct.Due_Date_5__c = System.Now().addHours(26);
ct.Due_Date_6__c = System.Now().addHours(28);
ct.Due_Date_7__c = System.Now().addHours(30);
ct.Due_Date_8__c = System.Now().addHours(31);
ct.Due_Date_9__c = System.Now().addHours(32);

ct.Priority_1__c =  'High';
ct.Priority_10__c =  'High';
ct.Priority_11__c =  'High';
ct.Priority_12__c =  'High';
ct.Priority_13__c =  'High';
ct.Priority_14__c =  'High';
ct.Priority_15__c =  'High';
ct.Priority_16__c =  'High';
ct.Priority_17__c =  'High';
ct.Priority_2__c =  'High';
ct.Priority_3__c =  'High';
ct.Priority_4__c =  'High';
ct.Priority_5__c =  'High';
ct.Priority_6__c =  'High';
ct.Priority_7__c =  'High';
ct.Priority_8__c =  'High';
ct.Priority_9__c =  'High';
ct.Status_1__c =  'Planned';
ct.Status_10__c =  'Planned';
ct.Status_11__c = 'Planned';
ct.Status_12__c = 'Planned';
ct.Status_13__c = 'Planned';
ct.Status_14__c = 'Planned';
ct.Status_15__c = 'Planned';
ct.Status_16__c = 'Planned';
ct.Status_17__c = 'Planned';
ct.Status_2__c = 'Auto Update';
ct.Status_3__c = 'Planned';
ct.Status_4__c = 'Planned';
ct.Status_5__c = 'Planned';
ct.Status_6__c = 'Planned';
ct.Status_7__c = 'Planned';
ct.Status_8__c = 'Planned';
ct.Status_9__c = 'Planned';
ct.Task_Subject_1__c = 'Confirm that there are 100 points of ID uploaded into the vault';
ct.Task_Subject_2__c = 'Confirm the customer is an authorised representative';
ct.Task_Subject_3__c = 'Confirm that the signed paperwork/voice recording is on file';
ct.Task_Subject_4__c = 'Confirm the Location Identification (LOC ID) of the address is correct in NBN portal';
ct.Task_Subject_5__c = 'Submit order in the DB OOT using details from the data sheet';
ct.Task_Subject_6__c = 'Upload generated contract summary into the documents folder in Commtrak and email order confirmation to customer.';
ct.Task_Subject_7__c = 'Check Quality Assurance is complete in the DB OOT';
ct.Task_Subject_8__c = 'Process the sale through ClickPOS and copy the invoice number into the workflow';
ct.Task_Subject_9__c = 'Follow up the order in DB OOT to see if the order is built';
ct.Task_Subject_10__c = 'Confirm order build is complete in DB OOT or by calling premise support';
ct.Task_Subject_11__c = 'Confirm the NBN appointment date from connection manager and copy date into workflow';
ct.Task_Subject_12__c = 'Call to advise the customer of date, follow up with email';
ct.Task_Subject_13__c = 'Confirm the Telstra appointment date';
ct.Task_Subject_14__c = 'Call to advise the customer of date, follow up with email';
ct.Task_Subject_15__c = 'Confirm with workforce that the appointment will go ahead on the day';
ct.Task_Subject_16__c = 'Set call back to check with customer that order is complete';
ct.Task_Subject_17__c = 'Set time for NPS email to be sent';
  
       
       insert ct;
       }     
      }
      
  }
}