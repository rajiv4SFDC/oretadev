public class opportunityTriggerHandler{

public static boolean afterUpdateRecurssion = false;



    /***********************************************************************    
    Method Name : beforeUpdateHandler
    written by  : Rekha
    Purpose : To through validation error on opportunity. When an opportunity is closed won, but if any opportunity product is in draft stage.
    Then validation should be fired with the Opportunity Product name.    
    **********************************************************************/
    
    public static void beforeUpdateHandler(list<opportunity> newList,map<id,opportunity> OldMap){
    
        set<id> oppIdSet = new set<id>();
    
        for(opportunity opp : newList){
            if(opp.recordtypeId ==Label.Axia_Opportunity_RecordTypeId  && opp.stageName !=oldmap.get(opp.id).stagename && opp.stagename=='Closed Won'){
                oppIdSet.add(opp.id);
            }
        }
        
        map<id,string> oppProdNameMap = new map<id,string>();
        for(Opportunity_Product__c oppProd : [SELECT id,Status__c,name,Opportunity__c  FROM Opportunity_Product__c WHERE Opportunity__c IN : oppIdSet and Status__c != 'Final']){
            if(!oppProdNameMap.isEmpty() && oppProdNameMap.containskey(oppProd.Opportunity__c) && oppProdNameMap.get(oppProd.Opportunity__c)!=NULL)
                oppProdNameMap.put(oppProd.Opportunity__c,oppProdNameMap.get(oppProd.Opportunity__c)+','+oppProd.name);
            else
                oppProdNameMap.put(oppProd.Opportunity__c,oppProd.name);       
        }
        
        for(opportunity opp : newList){
            if(opp.recordtypeId ==Label.Axia_Opportunity_RecordTypeId  && opp.stageName !=oldmap.get(opp.id).stagename && opp.stagename=='Closed Won'){
                if(!oppProdNameMap.isEmpty() && oppProdNameMap.containskey(opp.id) && oppProdNameMap.get(opp.id)!=NULL)
                    opp.addError('Kindly fill data sheets for '+oppProdNameMap.get(opp.id));
            }
        }    
    
    }



  public static void afterUpdateHandler(list<opportunity> newList, map<id,opportunity> OldMap)
  {
     if(afterUpdateRecurssion == false)
     {
     
        // process to create the tasks under the opportnity's custom object record called "Opportnity product". 
     
         set<string> oppIdsToProcessSet = new set<string>();
         map<string,string> oppIdWithOwnerIdMap = new map<string,string>();
         string oppownerId = '';
         string oppId ='';
        for(opportunity op1:newList)
          {
             if(op1.recordtypeId ==Label.Axia_Opportunity_RecordTypeId && op1.stageName != Null && op1.stageName=='Closed Won' && op1.stageName != oldMap.get(op1.id).stageName)
               {
                  oppIdsToProcessSet.add(op1.id);
                  oppIdWithOwnerIdMap.put(op1.id,op1.ownerId);
                  oppownerId = op1.OwnerId;
               }
          }
          
          if(oppIdsToProcessSet.size()>0)
          {
             // get all the custom opportunity product recordIds and create tasks for the same opp product records. 
             // get the list of tasks to be created from the selected lookup master product related list called Master Task info object. 
             map<string,string> oppProductIdWithMasterProdMap = new map<string,string>();
             map<string,Opportunity_Product__c> productWithoppProdIdMap = new map<string,Opportunity_Product__c>();
             list<task> tasksToInsert = new list<task>();
             map<string,string> OppProductIdWithOppIdMap = new map<string,string>();
             
             for(Opportunity_Product__c op:[select id,name,Opportunity__c,Opportunity__r.Name,Opportunity__r.Account.Name,Product__c,opp_owner_id__c from Opportunity_Product__c where Opportunity__c in:oppIdsToProcessSet])
             {
               oppProductIdWithMasterProdMap.put(op.id,op.Product__c);
               productWithoppProdIdMap.put(op.Product__c,op);
               OppProductIdWithOppIdMap.put(op.id,op.Opportunity__c);
             }
             
             if(productWithoppProdIdMap.keyset().size()>0)
               {
                 for(Master_Task_info__c mti:[select id,Name,Product__c,User__c,Product__r.Name,Task_Subject__c,Task_Status__c,Task_Priority__c,Task_Due_Date__c,Task_Serial_No__c from Master_Task_info__c where product__c in:productWithoppProdIdMap.keyset()])
                 {
                        task t1 = new task();
                        t1.subject = mti.Task_Subject__c;
                        t1.ActivityDate = Date.Today().addDays(integer.valueof(mti.Task_Due_Date__c));
                      // t1.OwnerId = productWithoppProdIdMap.get(mti.Product__c).opp_owner_id__c;
                      t1.OwnerId = mti.User__c;
                        t1.Priority = mti.Task_Priority__c; 
                        t1.WhatId = productWithoppProdIdMap.get(mti.Product__c).Id;
                          t1.Status = mti.Task_Status__c;
                        t1.Serial_No__c = mti.Task_Serial_No__c;
                        t1.Opportunity_Name__c = productWithoppProdIdMap.get(mti.Product__c).Opportunity__r.Name;
                        t1.Product_Name__c =  mti.Product__r.Name;
                        t1.Account_Name__c = productWithoppProdIdMap.get(mti.Product__c).Opportunity__r.Account.Name;
                        tasksToInsert.add(t1);
                        
                 }
               }
               system.debug('Tasks to insert is:'+tasksToInsert);
                  if(tasksToInsert.size()>0)
                     insert tasksToInsert;
               system.debug('Tasks to insert is 2 :'+tasksToInsert);

             
          }
     
     
     
     }
   
  
  }


}