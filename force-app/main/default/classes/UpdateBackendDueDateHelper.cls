public class UpdateBackendDueDateHelper {
    public static void updateBackendDueDate(list<Task> taskList){
        Date d;
        DateTime d1 = system.now();
        list<Task> newTaskList = new list<Task>();
        for(Task task : taskList){
            Task t = new Task(Id = task.Id);
            if(t.ActivityDate != null){
                d = t.ActivityDate;
            	task.backend_due_date__c = DateTime.newInstance(d.year(), d.month(), d.day(), d1.hour(), d1.minute(), d1.second()); 
                newTaskList.add(t);
            }
        }
        if(!newTaskList.isEmpty()){
     		update newTaskList;       
        }
    }
}