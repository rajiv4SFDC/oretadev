public class opportunityTriggerHandlerClosewon{
Public static void beforeUpdate(list<opportunity> newList){
    set<Id> Oid = new set<id>();
    for (Opportunity o: newList) {
        Oid.add(o.id);
    }
    List<Opportunity_Product__c> OLI = [Select id, name, Contract_Term__c,Total_Cost_Per_Month__c, Description__c from Opportunity_Product__c where Opportunity__c =: Oid];
    for (Opportunity o: newList) {
        for(Opportunity_Product__c ol : OLI){
            if(o.StageName=='Closed Won' && ol.Contract_Term__c==Null && ol.Description__c==Null && ol.Total_Cost_Per_Month__c==Null)  {
                o.StageName.addError('Fill your datasheet to make an opportunity close won');
            }
        }
    }
 }
}