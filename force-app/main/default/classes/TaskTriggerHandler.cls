public class TaskTriggerHandler{
    //This method is used to collect all those Expense's information which are getting inserted to calculate total expense at Campaign.
    public static void doHandleInsertExpense(List<Task> pListExpense){
        Set<id> parentSet = new Set<id>();
        for(Task theExp : pListExpense){
            System.debug('Campaign Id ='+theExp.WhatId);
            if(theExp.WhatId != NULL){
                System.debug('Coming to exp='+theExp);
                parentSet.add(theExp.WhatId);
            }
        }
        System.debug('Coming to the parent set='+parentSet);
        if(!parentSet.isEmpty()){
            doUpdateTotalExpense(parentSet);
        }
    }
    
    //This method is used to collect all those Expense's information which are getting updated to calculate total expense at Campaign.
    public static void doHandleUpdateExpense(List<Task> pListExpense, Map<Id, Task> pOldMapExp){
        Set<id> parentSet = new Set<id>();
        for(Task theExp : pListExpense){
            if(theExp.WhatId != NULL){
                parentSet.add(theExp.WhatId);
                if(pOldMapExp.get(theExp.Id).WhatId != NULL && theExp.WhatId != pOldMapExp.get(theExp.Id).WhatId){
                    parentSet.add(pOldMapExp.get(theExp.Id).WhatId);
                }
            }
        }
        if(!parentSet.isEmpty()){
            doUpdateTotalExpense(parentSet);
        }
    }
    //This method is used to collect all those Expense's information which are getting deleted to calculate total expense at Campaign.
    public static void doHandleDeleteExpense(List<Task> pOldListExp){
        Set<Id> parentSet = new Set<Id>();
        for(Task theExp : pOldListExp){
            if(theExp.WhatId != NULL){
                parentSet.add(theExp.WhatId);
            }
        }
        if(!parentSet.isEmpty()){
            doUpdateTotalExpense(parentSet);
        }
    }
    //This method is used to Perform calculation of Total Expense based on the Campaign Id's sent from doHandleInsertExpense(), doHandleUpdateExpense() and doHandleDeleteExpense() methods. 
    public static void doUpdateTotalExpense(Set<Id> pParentSet){
        Set<Id> lastCamSet = new Set<Id>();
        List<Opportunity_Product__c> toUpdateCampaigns = new List<Opportunity_Product__c>();
        for(AggregateResult agr : [SELECT  Count(id) totalExp, WhatId FROM Task WHERE WhatId IN : pParentSet  GROUP BY WhatId ]){
            Opportunity_Product__c theCam = new Opportunity_Product__c();
            theCam.Id = (Id)agr.get('WhatId');
            lastCamSet.add(theCam.Id);
            theCam.Sum_Of_Total_Task__c= (Decimal)agr.get('totalExp');
            toUpdateCampaigns.add(theCam);
            
        }
        
        for(Id Ids : pParentSet){
            if(!lastCamSet.contains(Ids)){
                Opportunity_Product__c led = new Opportunity_Product__c(Id = Ids, Sum_Of_Total_Task__c= 0);
                toUpdateCampaigns.add(led);
            }   
        }
        if(!toUpdateCampaigns.isEmpty()){
            update toUpdateCampaigns;
        }   
    }
}