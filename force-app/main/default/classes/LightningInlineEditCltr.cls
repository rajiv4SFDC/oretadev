public with sharing class LightningInlineEditCltr {
    @AuraEnabled
    public static List < task > accountsToDisplay(string recordId) {
       // system.debug('opportunity Id : '+opporunityDetails(recordId));
        List < task > returnList = new List < task > ();
        Date d=date.today();
        List < task > lstOfAccount = [select Id,Subject,Status,Description,ActivityDate,OwnerId,Owner.Name,Priority,WhatId,Serial_No__c,Opportunity_Name__c,backend_due_date__c,Product_Name__c,Account_Name__c from task where  whatid =: recordId order by Serial_No__c ];
        for (task acc: lstOfAccount) {
            /*if(acc.Backend_Due_Date__c != null){
                timeLine.put(acc.id,date.today().daysBetween(acc.Backend_Due_Date__c.date()));
                system.debug('timaline : '+timeLine.get(acc.Id));
            }*/
            returnList.add(acc);
            system.debug('Assigned To Id : '+acc.Backend_Due_Date__c);
        }
       
        return returnList;
    }
    // method for update records after inline editing 
    @AuraEnabled
    public static List < task > saveAccount(List<task> listOfAccount) {
        update listOfAccount;
        system.debug('The listOfAccount is:'+listOfAccount);
        return listOfAccount;
    }
    // method for fetch picklist values dynamic 
    @AuraEnabled
    public static List < String > getselectOptions(sObject objObject, string fld) {
        List < String > allOpts = new list < String > ();
        Schema.sObjectType objType = objObject.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        list < Schema.PicklistEntry > values = fieldMap.get(fld).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        allOpts.sort();
        return allOpts;
    }
    @AuraEnabled
    public static String opporunityDetails(string recordId) {
        sObject obj = [select Opportunity__c from Opportunity_Product__c where id =: recordId and opportunity__c != null];
        return (obj!=null)?(String)obj.get('Opportunity__c'):null;
    }
    @AuraEnabled
    public static map<Id,Integer> timeLineValues() {
         map<id,Integer> timeLine = new map<id,Integer>();
         List < task > returnList = new List < task > ();
        Date d=date.today();
        List < task > lstOfAccount = [select Id,Subject,Status,Description,ActivityDate,OwnerId,Owner.Name,Priority,WhatId,Serial_No__c,Opportunity_Name__c,backend_due_date__c,Product_Name__c,Account_Name__c from task where  whatid='a0r9D0000009XpZQAU' order by Serial_No__c ];
        for (task acc: lstOfAccount) {
            if(acc.Backend_Due_Date__c != null){
                timeLine.put(acc.id,date.today().daysBetween(acc.Backend_Due_Date__c.date()));
                system.debug('timaline : '+timeLine.get(acc.Id));
            }
        }
        return timeLine;
    }
}