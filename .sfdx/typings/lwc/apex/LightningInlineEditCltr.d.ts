declare module "@salesforce/apex/LightningInlineEditCltr.accountsToDisplay" {
  export default function accountsToDisplay(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/LightningInlineEditCltr.saveAccount" {
  export default function saveAccount(param: {listOfAccount: any}): Promise<any>;
}
declare module "@salesforce/apex/LightningInlineEditCltr.getselectOptions" {
  export default function getselectOptions(param: {objObject: any, fld: any}): Promise<any>;
}
declare module "@salesforce/apex/LightningInlineEditCltr.opporunityDetails" {
  export default function opporunityDetails(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/LightningInlineEditCltr.timeLineValues" {
  export default function timeLineValues(): Promise<any>;
}
