declare module "@salesforce/resourceUrl/ColorCodeGreen" {
    var ColorCodeGreen: string;
    export default ColorCodeGreen;
}
declare module "@salesforce/resourceUrl/ColorCodeRed" {
    var ColorCodeRed: string;
    export default ColorCodeRed;
}
declare module "@salesforce/resourceUrl/ColorCodeYellow" {
    var ColorCodeYellow: string;
    export default ColorCodeYellow;
}
declare module "@salesforce/resourceUrl/assignto" {
    var assignto: string;
    export default assignto;
}
declare module "@salesforce/resourceUrl/checklist" {
    var checklist: string;
    export default checklist;
}
declare module "@salesforce/resourceUrl/mail" {
    var mail: string;
    export default mail;
}
declare module "@salesforce/resourceUrl/status" {
    var status: string;
    export default status;
}
declare module "@salesforce/resourceUrl/timer" {
    var timer: string;
    export default timer;
}
