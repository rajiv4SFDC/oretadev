declare module "@salesforce/label/c.Axia_Opportunity_RecordTypeId" {
    var Axia_Opportunity_RecordTypeId: string;
    export default Axia_Opportunity_RecordTypeId;
}
declare module "@salesforce/label/c.DOT_NBN_CheckListTask" {
    var DOT_NBN_CheckListTask: string;
    export default DOT_NBN_CheckListTask;
}
declare module "@salesforce/label/c.Mobile_PPN_TNB_NEW" {
    var Mobile_PPN_TNB_NEW: string;
    export default Mobile_PPN_TNB_NEW;
}
